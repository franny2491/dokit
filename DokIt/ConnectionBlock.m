//
//  ConnectionBlock.m
//  DokIt
//
//  Created by Armadillo on 09/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "ConnectionBlock.h"

@implementation ConnectionBlock

+ (ConnectionBlock *) sharedInstance {
    static dispatch_once_t _singletonPredicate;
    static ConnectionBlock *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[self alloc] init];
    });
    
    return _singleton;
}

- (NSDictionary *)jsonDictionarySyncFor:(NSString *)urlString withData:(NSString *)postString
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLCacheStorageAllowedInMemoryOnly
                                                       timeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *json;
    if(data){
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    }
    else {
        json = @{errorKey : [error description]};
    }
    
    return json;
}

- (void)jsonDictionaryFor:(NSString *)urlString withData:(NSString *)postString withCompletionBlock:(void(^)(NSDictionary *parsedDictionary)) completionBlock
{
    if(!postString){
        completionBlock(@{errorKey : @"No Post String"});
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLCacheStorageAllowedInMemoryOnly
                                                       timeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *res, NSData *data, NSError *error) {
        NSDictionary *json;
        if(!error){
            NSError *errorHandler;
            json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorHandler];
            if(errorHandler){
                json = @{errorKey : [errorHandler description]};
            }
        }
        else {
            json = @{errorKey : [error description]};
        }
        if(completionBlock){
            completionBlock(json);
        }
    }];
}

@end
