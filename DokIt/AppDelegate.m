//
//  AppDelegate.m
//  DokIt
//
//  Created by Armadillo on 08/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "AppDelegate.h"
#import "MainDokitWindowController.h"

@interface AppDelegate()

@property(nonatomic, strong)MainDokitWindowController *mainWindowController;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.mainWindowController = [[MainDokitWindowController alloc] initWithWindowNibName:@"MainDokitWindowController"];
	[self.mainWindowController showWindow:self];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
	return YES;
}

@end
