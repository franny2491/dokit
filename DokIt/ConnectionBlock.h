//
//  ConnectionBlock.h
//  DokIt
//
//  Created by Armadillo on 09/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionBlock : NSObject

+ (ConnectionBlock *) sharedInstance;

- (void)jsonDictionaryFor:(NSString *)urlString
                 withData:(NSString *)postString
      withCompletionBlock:(void(^)(NSDictionary *parsedDictionary)) completionBlock;

- (NSDictionary *)jsonDictionarySyncFor:(NSString *)urlString
                               withData:(NSString *)postString;

@end
