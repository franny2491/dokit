//
//  NSDictionary+License.h
//  DokIt
//
//  Created by Armadillo on 12/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (License)

- (NSString *) postString;

@end
