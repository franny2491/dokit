//
//  NSDictionary+License.m
//  DokIt
//
//  Created by Armadillo on 12/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "NSDictionary+License.h"

@implementation NSDictionary (License)

- (NSString *) postString
{
    NSMutableString *returnString = [[NSMutableString alloc] initWithString:@""];
    for(int i = 0; i < [self.allKeys count]; i++) {
        NSString *key = [self.allKeys objectAtIndex:i];
        if(i == 0) {
            [returnString appendFormat:@"%@=%@", key, [self objectForKey:key]];
        }
        else {
            [returnString appendFormat:@"&%@=%@", key, [self objectForKey:key]];            
        }
    }
    return [returnString copy];
}

@end
