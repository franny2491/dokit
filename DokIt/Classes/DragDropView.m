//
//  DragDropView.m
//  DokIt
//
//  Created by Armadillo on 01/09/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "DragDropView.h"
#import "NSView+SNHelper.h"

@interface DragDropView()

@property(nonatomic, strong)NSMutableArray *dokitFileViewArray;

@property(nonatomic, assign)BOOL isHighlighted;

@end

@implementation DragDropView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        [self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
        
        self.dokitFileViewArray = [@[] mutableCopy];
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor whiteColor] setFill];
    NSRectFill(dirtyRect);
    
    if (self.isHighlighted) {
        [[NSColor grayColor] set];
        [NSBezierPath setDefaultLineWidth:5];
        [NSBezierPath strokeRect:[self bounds]];
    }
}

- (void)draggingExited:(id <NSDraggingInfo>)sender{
    self.isHighlighted = NO;
    [self setNeedsDisplay:YES];
}

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
    self.isHighlighted = NO;
    [self setNeedsDisplay:YES];
    return YES;
}

- (NSDragOperation)draggingEntered:(id )sender
{
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
    
    self.isHighlighted = YES;
    [self setNeedsDisplay:YES];
    
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType]) {
        if (sourceDragMask & NSDragOperationLink) {
            return NSDragOperationLink;
        }
        else if (sourceDragMask & NSDragOperationCopy) {
            return NSDragOperationCopy;
        }
    }
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id )sender
{
    NSPasteboard *pboard = [sender draggingPasteboard];
    
    if ([[pboard types] containsObject:NSFilenamesPboardType]) {
        NSURL *fileURL = [NSURL URLFromPasteboard:[sender draggingPasteboard]];
        
        DokitFileView *dokitFile = (DokitFileView *)[NSView loadFromNibWithName:@"DokitFileView"];
        [dokitFile setupWithFileRepresentation:fileURL withDelegate:self];
        [self addDokitFileViewToView:dokitFile];
    }
    return YES;
}

- (void)addDokitFileViewToView:(DokitFileView *)dokitFileView
{
    dokitFileView.frame = NSMakeRect((dokitFileView.frame.size.width * [self.dokitFileViewArray count]), 0, dokitFileView.frame.size.width, dokitFileView.frame.size.height);
    
    [self.dokitFileViewArray addObject:dokitFileView];
    [self addSubview:dokitFileView];
}

- (void)dokitDidGetDeleted:(DokitFileView *)dokitFileView
{
    // Remove File
    NSInteger positionToDelete = 0;
    NSInteger xPosition = dokitFileView.frame.origin.x;
    
    for(int i = 0; i < [self.dokitFileViewArray count]; i++){
        DokitFileView *dokitFile = self.dokitFileViewArray[i];
        if([dokitFileView isEqual:dokitFile]){
            positionToDelete = i;
            break;
        }
    }
    
    [[dokitFileView animator] setAlphaValue:0];
    [dokitFileView removeFromSuperviewAnimated];

    [self.dokitFileViewArray removeObjectAtIndex:positionToDelete];
    
    // Update all the ones to the right
    for(DokitFileView *fileView in self.dokitFileViewArray){
        if(fileView.frame.origin.x > xPosition){
            [[fileView animator] setFrame:NSMakeRect(fileView.frame.origin.x - fileView.frame.size.width,
                                                     fileView.frame.origin.y,
                                                     fileView.frame.size.width,
                                                     fileView.frame.size.height)];
        }
    }
}

@end
