//
//  MainDokitWindowController.h
//  DokIt
//
//  Created by Armadillo on 25/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainDokitWindowController : NSWindowController

- (void)addDokitRemoveFromArray:(id)sender;

@end
