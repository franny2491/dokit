//
//  SearchDokitWindowController.m
//  DokIt
//
//  Created by Armadillo on 05/08/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "SearchDokitWindowController.h"
#import "TagsAddedToDokit.h"
#import "NSAlert+Helper.h"

static TagsAddedToDokit *menuToken = nil;

@interface SearchDokitWindowController()

@property(nonatomic, weak)IBOutlet NSTokenField *tagField;

@property(nonatomic, strong)NSMenu *individualTagMenu;
@property(nonatomic, strong)NSString *tokenTitleToAdd;

@end

@implementation SearchDokitWindowController

#pragma mark - TokenField Methods

- (IBAction)editTagAction:(id)sender
{
	NSText *fieldEditor = [self.tagField currentEditor];
	NSRange textRange = [fieldEditor selectedRange];
	
	NSString *replacedString = [NSString stringWithString:menuToken.name];
	[fieldEditor replaceCharactersInRange:textRange withString:replacedString];
	[fieldEditor setSelectedRange:NSMakeRange(textRange.location, [replacedString length])];
}

- (IBAction)removeTagAction:(id)sender
{
    NSText *fieldEditor = [self.tagField currentEditor];
	NSRange textRange = [fieldEditor selectedRange];
	[fieldEditor replaceCharactersInRange:textRange withString:@""];
}

#pragma mark - NSTokenField Delegates

- (NSTokenStyle)tokenField:(NSTokenField *)tokenField styleForRepresentedObject:(id)representedObject
{
    NSTokenStyle returnStyle = NSPlainTextTokenStyle;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        returnStyle = NSRoundedTokenStyle;
    }
    return returnStyle;
}

- (BOOL)tokenField:(NSTokenField *)tokenField hasMenuForRepresentedObject:(id)representedObject
{
    BOOL hasMenu = NO;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        hasMenu = YES;
    }
    return hasMenu;
}

- (NSMenu *)tokenField:(NSTokenField *)tokenField menuForRepresentedObject:(id)representedObject
{
    NSMenu *returnMenu = nil;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        menuToken = representedObject;
        returnMenu = self.individualTagMenu;
    }
    
    return returnMenu;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField completionsForSubstring:(NSString *)substring indexOfToken:(NSInteger)tokenIndex
    indexOfSelectedItem:(NSInteger *)selectedIndex
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, searchForDokitUrl];
    NSDictionary *postDiction = @{ searchForDokitStringKey : substring };
    
    NSDictionary *dictionaryReturned = [[ConnectionBlock sharedInstance] jsonDictionarySyncFor:urlString withData:[postDiction postString]];
    
    if([dictionaryReturned objectForKey:errorKey]){
        [NSAlert showAlertFor:@"Error" andText:[NSString stringWithFormat:@"Error with searching for DokIt\n%@", [dictionaryReturned objectForKey:errorKey]]];
        return @[];
    }
    
    NSMutableArray *tagNamesReturned = [@[] mutableCopy];
    NSArray *arrayOfObjects = [dictionaryReturned objectForKey:dataKey];
    for(NSDictionary *dictionary in arrayOfObjects){
        [tagNamesReturned addObject:[dictionary objectForKey:addNewTagFileKey]];
    }
    
    return tagNamesReturned;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField shouldAddObjects:(NSArray *)tokens atIndex:(NSUInteger)index
{
    NSMutableArray *arrayOfTags = [tokens mutableCopy];
    
	for (id aToken in arrayOfTags)
	{
        if ([[aToken description] isEqualToString:self.tokenTitleToAdd])
		{
			TagsAddedToDokit *tag = [[TagsAddedToDokit alloc] init];
			tag.name = [aToken description];
            if(index > [arrayOfTags count] -1){
                [arrayOfTags addObject:tag];
            }
            else {
                [arrayOfTags replaceObjectAtIndex:index withObject:tag];
            }
			break;
		}
	}
    
    return arrayOfTags;
}

- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject
{
    NSString *returnString = nil;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        TagsAddedToDokit *tagAdded = (TagsAddedToDokit *)representedObject;
        returnString = tagAdded.name;
    }
    
    return returnString;
}

- (id)tokenField:(NSTokenField *)tokenField representedObjectForEditingString:(NSString *)editingString
{
    TagsAddedToDokit *dokitTag = [[TagsAddedToDokit alloc] init];
    dokitTag.name = editingString;
    return dokitTag;
}


@end
