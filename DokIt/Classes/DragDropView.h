//
//  DragDropView.h
//  DokIt
//
//  Created by Armadillo on 01/09/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DokitFileView.h"

@interface DragDropView : NSView <DokitFileViewDelegate>

- (void)addDokitFileViewToView:(DokitFileView *)dokitFileView;

@end
