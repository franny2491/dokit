//
//  DokitFileView.m
//  DokIt
//
//  Created by Armadillo on 02/09/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "DokitFileView.h"

@interface DokitFileView()

@property(nonatomic, weak)IBOutlet NSTextField *fileNameLabel;

@property(nonatomic, weak)id<DokitFileViewDelegate> delegate;

@end

@implementation DokitFileView

- (void)setupWithFileRepresentation:(NSURL *)withFilePath withDelegate:(id<DokitFileViewDelegate>)delegate;
{
    self.delegate = delegate;
    
    NSArray *arrayOfFileComponents = [[withFilePath path] componentsSeparatedByString:@"/"];
    self.fileNameLabel.stringValue = [arrayOfFileComponents lastObject];
}

- (IBAction)deleteFileFromAttaching:(id)sender
{
    [self.delegate dokitDidGetDeleted:self];
}

- (void)removeFromSuperviewAnimated
{
    [self performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.5];
}

@end
