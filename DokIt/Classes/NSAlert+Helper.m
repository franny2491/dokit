//
//  NSAlert+Helper.m
//  DokIt
//
//  Created by Armadillo on 25/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "NSAlert+Helper.h"

@implementation NSAlert (Helper)

+ (void)showAlertFor:(NSString *)messageText andText:(NSString *)informativeText
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert setMessageText:messageText];
    [alert setInformativeText:informativeText];
    [alert addButtonWithTitle:@"Ok"];
    [alert runModal];
}

@end
