//
//  MainDokitWindowController.m
//  DokIt
//
//  Created by Armadillo on 25/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "MainDokitWindowController.h"
#import "AddDokitWindowController.h"
#import "SearchDokitWindowController.h"
#import "NSAlert+Helper.h"

@interface MainDokitWindowController ()

@property(nonatomic, strong)SearchDokitWindowController *searchDokitWindowController;
@property(nonatomic, strong)NSMutableArray *arrayOfControllers;

@end

@implementation MainDokitWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    self.arrayOfControllers = [@[] mutableCopy];
    
    self.searchDokitWindowController = [[SearchDokitWindowController alloc] initWithWindowNibName:@"SearchDokitWindowController"];
}

- (IBAction)addDokit:(id)sender
{
    AddDokitWindowController *addDokitWindowController = [[AddDokitWindowController alloc] initWithWindowNibName:@"AddDokitWindowController"];
    [addDokitWindowController showWindow:self];
    addDokitWindowController.mainWindowController = self;
    [self.arrayOfControllers addObject:addDokitWindowController];
}

- (IBAction)searchDokit:(id)sender
{
    [self.searchDokitWindowController showWindow:self];
}

- (void)addDokitRemoveFromArray:(id)sender
{
    [self.arrayOfControllers removeObjectIdenticalTo:sender];
}

@end
