//
//  AddDokitWindowController.m
//  DokIt
//
//  Created by Armadillo on 25/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "AddDokitWindowController.h"
#import "MainDokitWindowController.h"
#import "NSAlert+Helper.h"
#import "TagsAddedToDokit.h"
#import "DokitFileView.h"
#import "DragDropView.h"
#import "NSView+SNHelper.h"
#import "PermissionButton.h"

static TagsAddedToDokit *menuToken = nil;

@interface AddDokitWindowController () <NSTokenFieldDelegate>

@property(nonatomic, weak)IBOutlet NSTokenField *tagField;
@property(nonatomic, weak)IBOutlet NSTextField *documentNameTextField;
@property(nonatomic, weak)IBOutlet NSTextField *descriptionTextField;
@property(nonatomic, weak)IBOutlet NSTextField *notesTextField;
@property(nonatomic, weak)IBOutlet DragDropView *dokitDropView;
@property(nonatomic, weak)IBOutlet PermissionButton *permissionTypeButton;

@property(nonatomic, strong)NSMutableDictionary *dokitInfoDictionary;
@property(nonatomic, strong)NSMenu *individualTagMenu;
@property(nonatomic, strong)NSString *tokenTitleToAdd;

@end

@implementation AddDokitWindowController

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    self.dokitInfoDictionary = [@{} mutableCopy];
    
    _individualTagMenu = [[NSMenu alloc] initWithTitle:@""];
    [self.individualTagMenu insertItem:[[NSMenuItem alloc] initWithTitle:@"Edit..."
                                                                  action:@selector(editTagAction:)
                                                           keyEquivalent:@""] atIndex:0];
    
    [self.individualTagMenu insertItem:[[NSMenuItem alloc] initWithTitle:@"Permissions..."
                                                                  action:@selector(removeTagAction:)
                                                           keyEquivalent:@""] atIndex:1];
    
    [self.individualTagMenu insertItem:[[NSMenuItem alloc] initWithTitle:@"Remove"
                                                                  action:@selector(removeTagAction:)
                                                           keyEquivalent:@""] atIndex:2];
    
    [self.tagField setCompletionDelay:0.0];
    self.tagField.tokenizingCharacterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSButton *closeButton = [self.window standardWindowButton:NSWindowCloseButton];
    
    [closeButton setTarget:self];
    [closeButton setAction:@selector(closeThisWindow:)];
    
    self.permissionTypeButton.permissionType = 1;
}

- (void)closeThisWindow:(id)sender
{
    [self.mainWindowController addDokitRemoveFromArray:self];
}

#pragma mark - Connection Methods

- (IBAction)addNewDokit:(id)sender
{
    if(self.documentNameTextField.stringValue.length == 0){
        return;
    }
    
    if(self.descriptionTextField.stringValue.length == 0){
        return;
    }
    
    [self.dokitInfoDictionary setValue:self.documentNameTextField.stringValue forKey:addNewTagFileKey];
    
    [self.dokitInfoDictionary setValue:self.descriptionTextField.stringValue forKey:descriptionTagNameKey];
    
    for(TagsAddedToDokit *tags in self.tagField.objectValue){
        NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, addNewTagUrl];
        NSDictionary *postDiction = @{ addNewTagNameKey : tags.name,
                                       addNewTagFileKey : [self.dokitInfoDictionary objectForKey:addNewTagFileKey] };
        
        [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:[postDiction postString] withCompletionBlock:^(NSDictionary *parsedDictionary) {
            
            if([parsedDictionary objectForKey:errorKey]){
                [NSAlert showAlertFor:@"Error" andText:[NSString stringWithFormat:@"Error with Adding New Tag\n%@", [parsedDictionary objectForKey:errorKey]]];
                return;
            }
            
            NSArray *arrayOfKeys = [parsedDictionary allKeys];
            [NSAlert showAlertFor:@"Success" andText:[arrayOfKeys objectAtIndex:0]];
        }];
    }
}

#pragma mark - Button Methods

- (IBAction)browseButtonPushed:(id)sender
{
    // Create a File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable options in the dialog.
    [openDlg setCanChooseFiles:YES];
    [openDlg setAllowsMultipleSelection:TRUE];
    // Display the dialog box.  If the OK pressed,

    // process the files.
    if ( [openDlg runModal] == NSOKButton ) {
        // Gets list of all files selected
        NSArray *files = [openDlg URLs];
        [self performSelector:@selector(processFilesFromArray:) withObject:files afterDelay:0.1];
    }
}

- (void)processFilesFromArray:(NSArray *)filesArray
{
    for(int i = 0; i < [filesArray count]; i++ ) {
        // Do something with the filename.
        DokitFileView *dokitFileView = (DokitFileView *)[NSView loadFromNibWithName:@"DokitFileView"];
        [dokitFileView setupWithFileRepresentation:filesArray[i] withDelegate:self.dokitDropView];
        [self.dokitDropView addDokitFileViewToView:dokitFileView];
    }
}

- (IBAction)permissionChangeButtonPushed:(id)sender
{
    PermissionButton *button = (PermissionButton *)sender;
    switch (button.permissionType) {
        case 1:
            button.image = [NSImage imageNamed:@"AbovePerm"];
            button.permissionType ++;
            break;
            
        case 2:
            button.image = [NSImage imageNamed:@"BelowPerm"];
            button.permissionType ++;
            break;
            
        case 3:
            button.image = [NSImage imageNamed:@"FlatPerm"];
            button.permissionType ++;
            break;
            
        case 4:
            button.image = [NSImage imageNamed:@"EveryPerm"];
            button.permissionType = 1;
            break;
            
        default:
            break;
    }
}

#pragma mark - TokenField Methods

- (IBAction)editTagAction:(id)sender
{
	NSText *fieldEditor = [self.tagField currentEditor];
	NSRange textRange = [fieldEditor selectedRange];
	
	NSString *replacedString = [NSString stringWithString:menuToken.name];
	[fieldEditor replaceCharactersInRange:textRange withString:replacedString];
	[fieldEditor setSelectedRange:NSMakeRange(textRange.location, [replacedString length])];
}

- (IBAction)removeTagAction:(id)sender
{
    NSText *fieldEditor = [self.tagField currentEditor];
	NSRange textRange = [fieldEditor selectedRange];
	[fieldEditor replaceCharactersInRange:textRange withString:@""];
}

#pragma mark - NSTokenField Delegates

- (NSTokenStyle)tokenField:(NSTokenField *)tokenField styleForRepresentedObject:(id)representedObject
{
    NSTokenStyle returnStyle = NSPlainTextTokenStyle;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        returnStyle = NSRoundedTokenStyle;
    }
    return returnStyle;
}

- (BOOL)tokenField:(NSTokenField *)tokenField hasMenuForRepresentedObject:(id)representedObject
{
    BOOL hasMenu = NO;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        hasMenu = YES;
    }
    return hasMenu;
}

- (NSMenu *)tokenField:(NSTokenField *)tokenField menuForRepresentedObject:(id)representedObject
{
    NSMenu *returnMenu = nil;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        menuToken = representedObject;
        returnMenu = self.individualTagMenu;
    }

    return returnMenu;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField completionsForSubstring:(NSString *)substring indexOfToken:(NSInteger)tokenIndex
    indexOfSelectedItem:(NSInteger *)selectedIndex
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, searchForTagUrl];
    NSDictionary *postDiction = @{ searchForTagKey : substring };
    
    NSDictionary *dictionaryReturned = [[ConnectionBlock sharedInstance] jsonDictionarySyncFor:urlString withData:[postDiction postString]];
    
    NSMutableArray *tagNamesReturned = [@[] mutableCopy];
    NSArray *arrayOfObjects = [dictionaryReturned objectForKey:dataKey];
    for(NSDictionary *dictionary in arrayOfObjects){
        [tagNamesReturned addObject:[dictionary objectForKey:addNewTagNameKey]];
    }
    
    return tagNamesReturned;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField shouldAddObjects:(NSArray *)tokens atIndex:(NSUInteger)index
{
    NSMutableArray *arrayOfTags = [tokens mutableCopy];
    
	for (id aToken in arrayOfTags)
	{
        if ([[aToken description] isEqualToString:self.tokenTitleToAdd])
		{
			TagsAddedToDokit *tag = [[TagsAddedToDokit alloc] init];
			tag.name = [aToken description];
            if(index > [arrayOfTags count] -1){
                [arrayOfTags addObject:tag];
            }
            else {
                [arrayOfTags replaceObjectAtIndex:index withObject:tag];
            }
			break;
		}
	}
    
    return arrayOfTags;
}

- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject
{
    NSString *returnString = nil;
    
    if([representedObject isKindOfClass:[TagsAddedToDokit class]]){
        TagsAddedToDokit *tagAdded = (TagsAddedToDokit *)representedObject;
        returnString = tagAdded.name;
    }
    
    return returnString; 
}

- (id)tokenField:(NSTokenField *)tokenField representedObjectForEditingString:(NSString *)editingString
{
    TagsAddedToDokit *dokitTag = [[TagsAddedToDokit alloc] init];
    dokitTag.name = editingString;
    return dokitTag;
}

@end