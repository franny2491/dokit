//
//  DokitFileView.h
//  DokIt
//
//  Created by Armadillo on 02/09/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol DokitFileViewDelegate;

@interface DokitFileView : NSView

- (void)setupWithFileRepresentation:(NSURL *)withFilePath withDelegate:(id<DokitFileViewDelegate>)delegate;
- (void)removeFromSuperviewAnimated;

@end

@protocol DokitFileViewDelegate <NSObject>

- (void)dokitDidGetDeleted:(DokitFileView *)dokitFileView;

@end
