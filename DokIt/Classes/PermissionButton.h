//
//  PermissionButton.h
//  DokIt
//
//  Created by Armadillo on 03/09/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PermissionButton : NSButton

@property(nonatomic, assign)NSInteger permissionType;

@end
