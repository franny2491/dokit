//
//  TagsAddedToDokit.h
//  DokIt
//
//  Created by Armadillo on 29/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagsAddedToDokit : NSObject

@property(nonatomic, strong)NSString *name;

@end
