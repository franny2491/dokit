//
//  NSAlert+Helper.h
//  DokIt
//
//  Created by Armadillo on 25/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSAlert (Helper)

+ (void)showAlertFor:(NSString *)messageText andText:(NSString *)informativeText;

@end
