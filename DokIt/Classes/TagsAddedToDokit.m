//
//  TagsAddedToDokit.m
//  DokIt
//
//  Created by Armadillo on 29/07/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "TagsAddedToDokit.h"

@implementation TagsAddedToDokit

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.name];
}

- (id)initWithCoder:(NSCoder *)decoder
{
	_name = [decoder decodeObject];
	return self;
}

@end
