//
//  Constants.h
//  DokIt
//
//  Created by Armadillo on 09/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#ifndef Mac_License_Constants_h
#define Mac_License_Constants_h

// API URLs
static NSString * const baseUrl = @"http://localhost:3000/v1"; // Dev
static NSString * const addNewTagUrl = @"/addTag";
static NSString * const searchForTagUrl = @"/searchForTag";
static NSString * const searchForDokitUrl = @"/searchForDokit";

// JSON Keys
static NSString * const addNewTagNameKey = @"tagName";
static NSString * const addNewTagFileKey = @"fileName";
static NSString * const searchForTagKey = @"searchTerm";
static NSString * const descriptionTagNameKey = @"description";
static NSString * const searchForDokitStringKey = @"dokitString";

static NSString * const dataKey = @"Data";
static NSString * const errorKey = @"error";

// NSArrayController Keys
static NSString * const arrayIsValidKey = @"isValid";
static NSString * const arrayNameLicenseKey = @"nameForLicense";

#endif
