//
//  main.m
//  DokIt Tests
//
//  Created by Armadillo on 09/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"GHUnitIOSAppDelegate");
    }
}
