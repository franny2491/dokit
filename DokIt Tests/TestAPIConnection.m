//
//  TestAPIConnection.m
//  DokIt
//
//  Created by Armadillo on 09/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "TestAPIConnection.h"

@implementation TestAPIConnection

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (BOOL)shouldRunOnMainThread
{
    return YES;
}

//  Commented out for now as this will delete all the database which we don't want to happen!
- (void)testdeleteAllEntries
{
    [self prepare];
    NSString *postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        NSString *urlNewString = [NSString stringWithFormat:@"%@%@", baseUrl, deleteLicenseUrl];
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        for(NSDictionary *diction in array){
            NSString *dataString = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@", bundleIDKey, [diction objectForKey:bundleIDKey], licenseKey, [diction objectForKey:licenseKey], nameKey, [diction objectForKey:nameKey]];
            [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlNewString withData:dataString withCompletionBlock:^(NSDictionary *parsedDictionary) {
                NSLog(@"%@", parsedDictionary);
            }];
        }
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testdeleteAllEntries)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
}

- (void)testBaseURL
{
    [self prepare];
    NSString *postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testBaseURL)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = @"";
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testBaseURL)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
}

- (void)testaddLicenseAndDelete
{
    [self prepare];
    NSString *postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick", bundleIDKey];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, addLicenseUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertEqualObjects([parsedDictionary objectForKey:errorKey], @"All parameters needed", @"Should show error for not both parameters");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testaddLicenseAndDelete)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        GHAssertTrue([[parsedDictionary objectForKey:hasCreatedKey] boolValue], @"ParsedDictionary should return created");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testaddLicenseAndDelete)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        BOOL hasFound = NO;
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.frederick"]){
                hasFound = YES;
                break;
            }
        }
        GHAssertTrue(hasFound, @"Should have found the newly created license");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testaddLicenseAndDelete)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    // delete entry
    [self prepare];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, deleteLicenseUrl];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testaddLicenseAndDelete)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        BOOL hasFound = NO;
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.frederick"]){
                hasFound = YES;
                break;
            }
        }
        GHAssertFalse(hasFound, @"Should have not found the newly deleted license");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testaddLicenseAndDelete)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
}

- (void)testvalidateLicense
{
    [self prepare];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, addLicenseUrl];
    NSString *postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        GHAssertTrue([[parsedDictionary objectForKey:hasCreatedKey] boolValue], @"ParsedDictionary should return created");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testvalidateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        BOOL hasFound = NO;
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.frederick"]){
                hasFound = YES;
                break;
            }
        }
        GHAssertTrue(hasFound, @"Should have found the newly created license");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testvalidateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, invalidateLicenseUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.frederick"]){
                GHAssertFalse([[diction objectForKey:validLicenseJSONKey] boolValue], @"should now be invalid");
            }
        }
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testvalidateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, validateLicenseUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.frederick"]){
                GHAssertTrue([[diction objectForKey:validLicenseJSONKey] boolValue], @"should now be invalid");
            }
        }
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testvalidateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    // delete entry
    [self prepare];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, deleteLicenseUrl];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testvalidateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
}

- (void)testupdateLicense
{
    [self prepare];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseUrl, addLicenseUrl];
    NSString *postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        GHAssertTrue([[parsedDictionary objectForKey:hasCreatedKey] boolValue], @"ParsedDictionary should return created");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testupdateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];

    [self prepare];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, updateLicenseUrl];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.frederick&%@=111111&%@=Sami&%@=com.fbeasley.changed&%@=123&%@=Francis", bundleIDKey, licenseKey, nameKey, newBundleIDKey, newLicenseKey, newNameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        GHAssertTrue([[parsedDictionary objectForKey:changedJSONKey] boolValue], @"ParsedDictionary should return changed");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testupdateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    [self prepare];
    postString = [NSString stringWithFormat:@"%@=hcUqREnDc8mzuCRpK8YJVJ", passwordKey];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, getLicensesUrl];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        NSArray *array = [parsedDictionary objectForKey:dataKey];
        BOOL hasFound = NO;
        for(NSDictionary *diction in array){
            if([[diction objectForKey:bundleIDKey] isEqualToString:@"com.fbeasley.changed"]){
                hasFound = YES;
                break;
            }
        }
        GHAssertTrue(hasFound, @"Should have found the newly changed license");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testupdateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
    
    // delete entry
    [self prepare];
    urlString = [NSString stringWithFormat:@"%@%@", baseUrl, deleteLicenseUrl];
    postString = [NSString stringWithFormat:@"%@=com.fbeasley.changed&%@=123&%@=Francis", bundleIDKey, licenseKey, nameKey];
    [[ConnectionBlock sharedInstance] jsonDictionaryFor:urlString withData:postString withCompletionBlock:^(NSDictionary *parsedDictionary) {
        GHAssertNotNil(parsedDictionary, @"ParsedDictionary should return a valid Dictionary");
        [self notify:kGHUnitWaitStatusSuccess forSelector:@selector(testupdateLicense)];
    }];
    [self waitForStatus:kGHUnitWaitStatusSuccess timeout:10.0];
}

@end
