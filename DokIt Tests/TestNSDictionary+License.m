//
//  TestNSDictionary+License.m
//  DokIt
//
//  Created by Armadillo on 12/03/2013.
//  Copyright (c) 2013 FBeasley Software. All rights reserved.
//

#import "TestNSDictionary+License.h"
#import "NSDictionary+License.h"

@implementation TestNSDictionary_License

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (BOOL)shouldRunOnMainThread
{
    return YES;
}

- (void) testPostString
{
    NSDictionary *testDiction = @{};
    GHAssertEqualObjects([testDiction postString], @"", @"postString should return empty string for empty dictionary");
    
    testDiction = @{@"Fred" : @"25"};
    GHAssertEqualObjects([testDiction postString], @"Fred=25", @"postString should return correct setup for parameters");

    testDiction = @{@"Fred" : @"25", @"Sami" : @"29"};
    GHAssertEqualObjects([testDiction postString], @"Fred=25&Sami=29", @"postString should return correct setup for parameters");
}


@end
