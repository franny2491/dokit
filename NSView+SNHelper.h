//
//  NSView+SNHelper.h
//  SnakeGame
//
//  Created by Armadillo on 01/09/2013.
//  Copyright (c) 2013 FBeasleySoftware. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSView (SNHelper)

+ (NSView *)loadFromNibWithName:(NSString *)name;

@end
