//
//  NSView+SNHelper.m
//  SnakeGame
//
//  Created by Armadillo on 01/09/2013.
//  Copyright (c) 2013 FBeasleySoftware. All rights reserved.
//

#import "NSView+SNHelper.h"

@implementation NSView (SNHelper)

+ (NSView *)loadFromNibWithName:(NSString *)name
{
    NSNib *nib = [[NSNib alloc] initWithNibNamed:name bundle:nil];
    NSArray *topLevelObjects;
    if (![nib instantiateWithOwner:self topLevelObjects:&topLevelObjects]){
        
    }
        
    NSView *view = nil;
    for (id topLevelObject in topLevelObjects) {
        if ([topLevelObject isKindOfClass:[NSView class]]) {
            view = topLevelObject;
            break;
        }
    }
    
    return view;
}

@end
